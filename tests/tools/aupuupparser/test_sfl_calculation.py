import pytest

from src.tools.aupuupparser.rsa_area import RSAArea, RSAAreaTopSky
from src.tools.aupuupparser.rsa_notice import RSANotice


@pytest.mark.parametrize('flight_level', range(641, 999))
def test_returns_unl(flight_level):
    area = RSAArea(max_fl=flight_level)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert "UNL" in output['user_text']


@pytest.mark.parametrize('flight_level', range(31, 640))
def test_prefix_sfl(flight_level):
    area = RSAArea(max_fl=flight_level)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert "SFL" in output['user_text']


@pytest.mark.parametrize('flight_level', range(1, 30))
def test_prefix_sfa(flight_level):
    area = RSAArea(max_fl=flight_level)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert "SFA" in output['user_text']


@pytest.mark.parametrize('min_fl', range(15, 640))
def test_max(min_fl):
    area = RSAArea(min_fl=min_fl, max_fl=641)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert "MAX" in output['user_text']


def test_level_one_thousand_feet_separation_1():
    area = RSAArea(min_fl=0, max_fl=300)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert 'SFL310' in output['user_text']


def test_level_one_thousand_feet_separation_2():
    area = RSAArea(min_fl=0, max_fl=50)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert 'SFL060' in output['user_text']


def test_altitude_one_thousand_feet_separation_1():
    area = RSAArea(min_fl=0, max_fl=10)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert 'SFA2000' in output['user_text']


def test_altitude_one_thousand_feet_separation_2():
    area = RSAArea(min_fl=0, max_fl=30)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert 'SFA4000' in output['user_text']


def test_level_two_thousand_feet_separation_1():
    area = RSAArea(min_fl=0, max_fl=500)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert 'SFL520' in output['user_text']


def test_level_two_thousand_feet_separation_2():
    area = RSAArea(min_fl=0, max_fl=640)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert 'SFL660' in output['user_text']


def test_level_one_thousand_feet_separation_independent_from_minimum():
    area = RSAArea(min_fl=50, max_fl=500)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert 'SFL520' in output['user_text']


def test_area_with_airspace_bellow_1():
    area = RSAArea(min_fl=35, max_fl=700)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert 'SFA2500MAX' in output['user_text']


def test_area_with_airspace_bellow_2():
    area = RSAArea(min_fl=25, max_fl=700)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert 'SFA1500MAX' in output['user_text']


def test_area_with_airspace_bellow_3():
    area = RSAArea(min_fl=50, max_fl=800)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert 'SFA4000MAX' in output['user_text']


def test_area_with_airspace_bellow_4():
    area = RSAArea(min_fl=22, max_fl=700)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert 'SFA1200MAX' in output['user_text']


def test_returns_four_digit_altitude():
    area = RSAArea(min_fl=15, max_fl=700)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert 'SFA0500MAX' in output['user_text']


def test_returns_three_digit_level():
    area = RSAArea(min_fl=75, max_fl=800)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert 'SFL060MAX' in output['user_text']


def test_level_rounds_up():
    area = RSAArea(min_fl=0, max_fl=35)
    notice = RSANotice()
    output = RSAAreaTopSky(area, notice).output()
    assert 'SFL050' in output['user_text']
