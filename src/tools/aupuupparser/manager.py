from collections import Iterator

from src.tools.aupuupparser.parser import RSANoticeParser, RSARawDataParser, get_line_area_arguments
from src.tools.aupuupparser.rsa_area import RSAAreaTopSky, RSAArea
from src.tools.aupuupparser.rsa_notice import RSANotice

EXCLUDE_LIST = ['LPNEVER', 'LPR42B', 'LPSNOW']


class Manager:
    def __init__(self, form_rawdata: str, form_country_icao_code: str):
        self.icao_code = form_country_icao_code
        self.notice = RSANotice(**RSANoticeParser(form_rawdata).arguments)
        self.list_lines_with_code = RSARawDataParser(form_rawdata).get_lines_with_code(form_country_icao_code)

    def area_iterator(self, short_name: bool, user_text: bool, safe_level: bool) -> Iterator[str]:
        for line in self.list_lines_with_code:
            area = RSAArea(**get_line_area_arguments(line))
            if area.name not in EXCLUDE_LIST:
                top_sky_area = RSAAreaTopSky(area, self.notice)
                yield ":".join(top_sky_area.output(short_name, user_text, safe_level).values())
