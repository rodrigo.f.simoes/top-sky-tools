import re

import requests


def get_metar(params: str):
    params = params.replace(',', ' ')
    response = requests.post(f'https://brief-ng.ipma.pt/showopmetquery.php', data={"icaos": params, "type": "metar"})
    matches = re.findall(r'METAR (.*)', response.text)
    to_return = ""
    for match in matches:
        to_return += f"(prefix){match}(suffix)"
    return to_return
